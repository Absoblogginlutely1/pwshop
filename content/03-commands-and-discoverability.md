---
weight: 110
---

# Commands & Discoverability

When using PowerShell, the majority of the text you type into a console or script file will be PowerShell commands of some sort, instead of external executables.
For this reason, it's useful to become familiar with PowerShell's `Verb-Noun` syntax, approved verbs, and some of the most common commands.

> You can also retrieve the available verbs (though not their meanings) from the PowerShell prompt:

```powershell
Get-Verb
```
```text
Verb   Group
----   -----
Add    Common
Clear  Common
Close  Common
Copy   Common
Enter  Common
...
```

In Powershell every command follows the format `Verb-Noun`.
For example:

- `Get-Item`
- `Set-Item`
- `New-Item`
- `Remove-Item`

Those are all real commands and they do roughly what you think they do - get an item, set an item's value, create a new item, delete an item.

There's a [canonical list][approved-verbs] of approved verbs which PowerShell commands use.
This helps to build a context that makes understanding a command _without_ running it easier.
If you know what the verb means and the noun is clear, you have a pretty good guess at what's going on.
You wouldn't expect that a `Get-SomeObject` command would do anything but retrieve information, for example.

<aside class="success">
For the rest of this workshop, you may want to keep the list of approved verbs open.
</aside>

In addition to the verbs there's a few core commands that you should know and we're going to cover in this workshop:

- `Get-Command`
- `Get-Help`
- `Select-Object`

<aside class="notice">
<strong>Case Sensitivity</strong><br>
PowerShell is not itself case sensitive, so don't worry <em>too</em> much about getting the casing correct.
In PowerShell, we mostly use <a href="http://wiki.c2.com/?PascalCase">PascalCase</a>.
</aside>

## Get-Command

```powershell
Get-Command
```
```text
CommandType     Name                                               Version    Source
-----------     ----                                               -------    ------
Alias           Add-ProvisionedAppxPackage                         3.0        Dism
Alias           Add-ProvisioningPackage                            3.0        Provisioning
Alias           Add-TrustedProvisioningCertificate                 3.0        Provisioning
Alias           Apply-WindowsUnattend                              3.0        Dism
Alias           Disable-PhysicalDiskIndication                     2.0.0.0    Storage
Alias           Disable-StorageDiagnosticLog                       2.0.0.0    Storage
Alias           Enable-PhysicalDiskIndication                      2.0.0.0    Storage
Alias           Enable-StorageDiagnosticLog                        2.0.0.0    Storage
Alias           Export-VMCheckpoint                                2.0.0.0    Hyper-V
Alias           Flush-Volume                                       2.0.0.0    Storage
...
```

So we know that we're going to need to run commands in Powershell.
We also know that PowerShell use a predictable `Verb-Noun` structure for those commands.
But what we _don't_ know yet is how to find a command we might want to run.

For that we use `Get-Command`, whose [documentation can be found here][get-command].

We can use `Get-Command` to return the list of all available commands in our PowerShell session.
We can also filter those commands by their _type_, name, verb, noun, or several other options.

<aside class="notice">
<strong>Types of Commands</strong><br>
Commands have several types:

<ul>
<li>Alias</li>
<li>Application</li>
<li>Cmdlet</li>
<li>Function</li>
</ul>

<p>
For our purposes, the differences aren't <em>super</em> important.
Cmdlets are PowerShell commands defined in C# code, functions are defined in PowerShell themselves, and aliases are names that map to cmdlets or functions.
</p>
<p>
Applications are executables files that are on the Path and can be called from PowerShell.
You can call applications that <code>Get-Command</code> doesn't show, but you'll need to specify the full path to them.
</p>
</aside>

When we do this, we're passing a **parameter** to the command.
This is the most common way we're going to use commands at the prompt, by passing one or more parameters to the command.

With that in mind, our first exercise!

## Exercise 1: Using Parameters
### A: Single Parameter
```powershell
Get-Command -Verb Trace
Get-Command -verb TrAce
```
```text
CommandType     Name              Version    Source
-----------     ----              -------    ------
Cmdlet          Trace-Command     3.1.0.0    Microsoft.PowerShell.Utility
```

First, let's use the `Verb` parameter to filter for only those commands with the `Trace` verb - it's not used very much, so the list should be small.
Note that you must get the verb exactly correct.

What happens if you instead specify `traze` or `traces`?
What if you specify `tra*`?

### B: Multiple Parameters
```powershell
Get-Command -Noun Command -TotalCount 3
```
```text
CommandType     Name                Version    Source
-----------     ----                -------    ------
Cmdlet          Get-Command         3.0.0.0    Microsoft.PowerShell.Core
Cmdlet          Invoke-Command      3.0.0.0    Microsoft.PowerShell.Core
Cmdlet          Measure-Command     3.1.0.0    Microsoft.PowerShell.Utility
```

We can also filter for only those commands whose noun is `Command` and use the `TotalCount` parameter to only return the first three commands our query finds.
This will return all of the commands on our system that match - note that these commands include different sources.

What happens if you omit the `TotalCount` parameter?
How many results do you get back?

What happens if you specify a verb in addition to the noun?
How many results do you get back?

### C: Filtering Parameters
```powershell
Get-Command -Name *image*
```
```text
CommandType     Name                            Version    Source
-----------     ----                            -------    ------
Function        Dismount-DiskImage              2.0.0.0    Storage
Function        Get-DiskImage                   2.0.0.0    Storage
Function        Mount-DiskImage                 2.0.0.0    Storage
Cmdlet          Add-WindowsImage                3.0        Dism
...
```
But what if we don't know the exact noun being used?
In that case, we can filter on `Name` and try to narrow our options down.

If, for example, we want to see what commands are available for interacting with images, we could use wildcards (`*`) in conjunction with the `Name` parameter.

<aside class="warning">
<strong>Parameters and Wildcards</strong><br>
Not all parameters take wildcards; those that <strong>do</strong> will state so in their reference docs.
</aside>

What command would you run to narrow this down to commands for mounting and dismounting disk images?
For just Windows images?

What parameter would you use to return only commands that would get images, not act on them?

## Get-Help
```powershell
Get-Help -Name Get-Help
```
```text
NAME
    Get-Help

SYNOPSIS
    Displays information about Windows PowerShell commands and concepts.

...
```
In the previous section we looked at how to use commands and their parameters - but how do you discover _what_ parameters are available for a particular command?
And even if you know that a command has a parameter, how do you know what it accepts or expects?

Luckily, PowerShell has a pretty awesome built-in help system.

We can access that by running `Get-Help` and passing it a command or topic to get help on.
Let's first get help on `Get-Help` itself!

```powershell
Get-Help -Name Get-Help -ShowWindow
Get-Help -Name Get-Help -Online
```
This returns information similar to Linux [man pages](https://en.wikipedia.org/wiki/Man_page) - that is, it returns reference documentation for PowerShell commands.
You can get the help directly in the console, but you can also get the help in a pop out window or open a link to the online documentation (if it exists).

[approved-verbs]: https://docs.microsoft.com/en-us/powershell/developer/cmdlet/approved-verbs-for-windows-powershell-commands
[get-command]: https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/get-command?view=powershell-6

What parameter would you add to show examples? To show the full help?

## Exercise 2: Getting Help

### A: Getting Help for Get-Command
```powershell
Get-Help -Name Get-Command
```

Now that you know how to get help for a command, get the help for `Get-Command` in a pop-out window so you can reference it for the rest of this section.

What command did you use?

How many parameters does `Get-Command` have?

Which parameter would you use to limit the number of results?
To ensure that it only returns results from a particular module?

What does the `ListImported` parameter do?

## Exercise 3: Getting Commands and Help

### A: Service Management
Often when investigating issues on a server you'll need to see if a service is running or not.

1. What command would you use to query for commands that manage Windows services?
2. What is the command for querying the system for services?
3. What parameters does that command take?
4. What command would you run to check if the print spooler service is running?
5. Is the print spooler service running?
6. If it is running, what command would you use to stop it? (remember, you can look up commands with `Get-Command` whenever you want)
7. Make sure the service is stopped.
8. Start the service again.
9. Restart the service with a single command.
