---
weight: 150
---

# Variables & Operators

So far in this workshop we've worked through using commands at the prompt and manipulating the output.
But what if you need to store objects for later use?

This is where variables come in.
In PowerShell variables are strings with a `$` symbol prepended to them, such as `$Credential` or `$Results`.

You've already seen a special variable, `$_`, in the previous section.
This is an automatic variable provided by PowerShell and which contains the current object in the pipeline.
It has an alias, `$PSItem`.

```powershell
$SomeVariable = 5
Write-Output $SomeVariable
$SomeVariable = 3
Write-Output $SomeVariable
$SomeVariable = Get-Service | Select-Object -First 1 -ExpandProperty Name
Write-Output $SomeVariable
```
```text
5
3
AESMService
```

To assign a value to a variable we use the `=` symbol.
You can reassign a variable to another value if you wish.

The value to the right of the `=` can be any valid PowerShell command.

This is useful for storing long values or for caching the results of commands.
It becomes _very_ useful when writing scripts.

## Operators
There are several other [operators][operators] available to manipulate objects in PowerShell.
We've previously looked at comparison and logical operators.

You can add (`+`), subtract (`-`), multiply (`*`), divide (`/`), and calculate the remainder of a division operation (`%`).

Note that you can use the arithmetic operators on any object that supports them, such as `String` and `DateTime`, in addition to numbers.

## Exercise X: Arithmetic Operators

1. Set the value of the variable `$Example` equal to the string `Apple`.
2. What is the result of `$Example + 5`?
3. What is the result of `$Example - 1`?
  - What about `$Example.Length - 1`?
4. What is the result of `$Example * 2`?
5. What is the result of `$Example / 5`?
  - What about `$Example.Length % 3`?
6. Set the value of the variable `$Example` equal to `Get-Date`.
7. What is the result of `$Example + 1`?
  - What about `$Example + 1000`?
  - What about `$Example + 10000000`?
8. What methods are available on `$Example`?
9. Add 136 hours to `$Example` using the appropriate method.
  - What date is returned?
10. Specify `-136` instead of `136`, using the same method.
  - What date is returned?

<aside class="notice">
<strong>When Not to Use Arithmetic Operators</strong><br>
Sometimes it is easier to read and understand code that uses methods rather than the arithmetic operators.
This is especially true in cases where the arithmetic operators are being used in way that might not be easy to guess.
</aside>

## Assignment Operators

```powershell
$Data = 2
Write-Output $Data
$Data += 5
Write-Output $Data
$Data -= 1
Write-Output $Data
$Data *= 3
Write-Output $Data
$Data /= 2
Write-Output $Data
$Data %= 7
Write-Output $Data

```
```text
2
7
6
18
9
2
```

Now that you know the arithmetic operators you'll be able to make sense of the assignment operators.
You're already familiar with `=`, which sets the variable on the left equal to the output of the PowerShell on the right.

There is also the `+=` operator, which sets the variable on the left equal to itself _plus_ the output of the PowerShell on the right.
The `-=` operator sets the variable equal to itself _minus_ the output of the PowerShell on the right, the `/=` operator to itself _divided_ by the output of the PowerShell on the right, and so on.

## Unary Operators

```powershell
$a = 5
$a++
Write-Output $a
```
```text
6
```

You can increment or decrement variables and properties using the unary operators `++` and `--` respectively.
Note that this is functionaly equivalent to `+= 1` and `-= 1`.

[operators]: https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_operators?view=powershell-6