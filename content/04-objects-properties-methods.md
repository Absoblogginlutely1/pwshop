---
weight: 120
---

# Objects, Properties, & Methods

## Properties

```powershell
Get-Command -Noun Command
```
```text
CommandType     Name                Version    Source
-----------     ----                -------    ------
Cmdlet          Get-Command         3.0.0.0    Microsoft.PowerShell.Core
Cmdlet          Invoke-Command      3.0.0.0    Microsoft.PowerShell.Core
Cmdlet          Measure-Command     3.1.0.0    Microsoft.PowerShell.Utility
...
```

So far we've been looking at single commands and their output.
But why does `Get-Command` return something that looks like a table to the screen?

It does this because it's returning an array of _objects_ (as rows in the table output), each of which have _properties_ (seen as the columns in the output).

So, looking at the results, the only properties for this command object we're getting back are `CommandType`, `Name`, `Version`, and `Source`, right?

Nope! There's several more properties, these are just the ones you see by default.
As you're about to see, displaying all of the properties would be a bit of a mess visually.

```powershell
Get-Command -Name Get-Command | Get-Member -MemberType Properties
```
```
   TypeName: System.Management.Automation.CmdletInfo

Name                MemberType     Definition
----                ----------     ----------
CommandType         Property       System.Management.Automation.CommandTypes CommandType {get;}
DefaultParameterSet Property       string DefaultParameterSet {get;}
Definition          Property       string Definition {get;}
HelpFile            Property       string HelpFile {get;}
...
```
How do you discover all of the properties a command has?
You can use `Get-Member`.

<aside class="success">
<strong>Get Help</strong><br>
It's probably a good idea to open up the help for <code>Get-Member</code> in a pop out window. <br>

If you don't remember how, check above in the <a href="#get-help"><code>Get-Help</code></a> section.
</aside>

Notice that the first thing this command returned was a `TypeName`.
This is the fully qualified name of the type of object you're looking at.
In this case, the shorthand is `CmdletInfo`.

The next thing you'll see is a list of properties - `CommandType`, `Definition`, etc.

```powershell
Get-Command -Name Get-Command | Select-Object -Property Name, HelpFile
```
```text
Name        HelpFile
----        --------
Get-Command System.Management.Automation.dll-Help.xml
```

Okay, so now you can see what properties an object has - but how can you see the _values_ of those properties?

We can do that with `Select-Object`, a command which will be a commonly used part of your toolkit for the rest of this workshop.
Notice that we passed two properties to the `Select-Object` command: `Name` and `HelpFile`.
If you want to display all of the properties you can do so by specifying a wildcard `*` instead of any property names.

```powershell
Get-Command -Name Get-Command | Select-Object -Property *
```
```text
HelpUri             : https://go.microsoft.com/fwlink/?LinkID=113309
DLL                 : C:\WINDOWS\Microsoft.Net\assembly\GAC_MSIL\System.Management.Automation\v4.0_3.0.0.0__31bf3856ad364e35\System.Management.Automation.dll
Verb                : Get
Noun                : Command
...
```

This will display all of the same properties that an object had listed when you introspected it with the `Get-Member` command.

<aside class="notice">
<strong>Hidden Properties</strong><br>
Some objects will have <em>hidden</em> properties. You can use the <code>Force</code> parameter on <code>Select-Object</code> and <code>Get-Member</code> to display them. This won't <em>usually</em> be necessary.
</aside>

## Exercise X: Service Properties

Remember that you can use `Get-Service` to retrieve services; do so to retrieve the print spooler service.

1. What properties are available for a service object?
2. What type of service is the print spooler service?
3. What is the start type for the print spooler service?
4. Can the print spooler service stop?
5. Can it pause and continue?

## Methods

```powershell
Get-Command -Name Get-Command | Get-Member -MemberType Methods
```
```text
   TypeName: System.Management.Automation.CmdletInfo

Name             MemberType Definition
----             ---------- ----------
Equals           Method     bool Equals(System.Object obj)
GetHashCode      Method     int GetHashCode()
GetType          Method     type GetType()
ResolveParameter Method     System.Management.Automation.ParameterMetadata ResolveParameter(string name)
ToString         Method     string ToString()
```

In addition to the _properties_, just about every object also has methods.
Normally, in PowerShell, you will be able to find a command to manage an object (to start or stop a service, for example) - but, because PowerShell is built on top of .NET, you also have access to the methods directly.

You can find the methods for an object using `Get-Member` again, this time specifying `Methods`.
Each of these methods is [documented](https://docs.microsoft.com/en-us/dotnet/api/system.management.automation.cmdletinfo?view=powershellsdk-1.1.0), but the method we want to focus on right now is [`ResolveParameter`](https://docs.microsoft.com/en-us/dotnet/api/system.management.automation.commandinfo.resolveparameter?view=powershellsdk-1.1.0#System_Management_Automation_CommandInfo_ResolveParameter_System_String_).

Looking at the documentation for it we find that it can be passed the name of a commands parameter and will return metadata about it.

```powershell
(Get-Command -Name Get-Command).ResolveParameter('Something')
```
```text
Exception calling "ResolveParameter" with "1" argument(s): "A parameter cannot be found that matches parameter name 'Something'."
At line:1 char:1
+ (Get-Command -Name Get-Command).ResolveParameter('Something')
+ ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    + CategoryInfo          : NotSpecified: (:) [], MethodInvocationException
    + FullyQualifiedErrorId : ParameterBindingException
```

If we just pass an arbitrary string to the method PowerShell shows us an error.
It looks like `Something` isn't a valid parameter for `Get-Command`.
So what we need to do is discover what valid parameters `Get-Command` _does_ have.
We can do that with our old friend `Get-Help`.

```powershell
Get-Help -Name Get-Command -ShowWindow
```

Looking through the list of parameters, pick one - this example is going to use `Module`

```powershell
(Get-Command -Name Get-Command).ResolveParameter('Module')
```
```text
Name            : Module
ParameterType   : System.String[]
ParameterSets   : {[__AllParameterSets, System.Management.Automation.ParameterSetMetadata]}
IsDynamic       : False
Aliases         : {PSSnapin}
Attributes      : {__AllParameterSets, System.Management.Automation.AliasAttribute}
SwitchParameter : False
```

So that gives us back some interesting information about the `Module` parameter:

- We see its name, `Module`, but we already had that.
- We see that it accepts input which is one or more strings of characters - the `[]` denotes that it accepts an array.
- We see that it belongs to all parameter sets.
- We see that it is not dynamic.
- We see that it has one alias, `PSSnapin`, a holdover from when PowerShell commands were distributed as snapins instead of modules.
- We see that it has attributes for belonging to all parameter sets and for having an alias, both of which we would expect given the values in `ParameterSets` and `Aliases`.
- We see that it is _not_ a switch parameter, which makes sense as it accepts input and is not a toggle.

## Exercise X: File Methods
> Create a new file using the `New-Item` command.
> You can retrieve information about a file using the `Get-Item` command.
> We'll cover why this is `Item` and not `File` shortly!

```powershell
New-Item -Name 'something.txt' -Value 'just some text'
Get-Item -Path something.txt
```
```text
# NOTE: The directory and LastWriteTime will be different for you,
# depending on when you're doing the workshop and in which folder.

    Directory: C:\code\personal\pwshop
Mode                LastWriteTime         Length Name
----                -------------         ------ ----
-a----        9/18/2018   4:32 PM             14 something.txt
```

Running the `ResolveParameter` method on a command's information is interesting but not particularly useful.
What if we wanted to manage a file?

1. Retrieve information about `something.txt` using the `Get-Item` command to the right.
2. What methods are available on a file object?
3. What is the name of the method for copying the file?
4. Make a copy of the file as `else.txt` using that method.
8. Retrieve information about `else.txt`.
5. Use the `Delete` method on `something.txt`.
6. Retrieve information about `something.txt` again.
7. Did `Get-Item` error? Why?
8. Use the `Delete` method on `else.txt`.

## Output Views

Up to this point you may have noticed that sometimes PowerShell seems to output results as a table, sometimes as a list of properties and their values.
Actually, neither of these is true - PowerShell _always_ emits objects.
What you're seeing is a different _format_ for the output.

There's a few different commands you can do to change the output format:

- `Format-Table` displays the output in a table, which is what we're used to seeing.
- `Format-List` displays the output in a list of key-value pairs.
- `Format-Wide` displays only one property of an object (and most objects have a default).
- `Out-Gridview` isn't strictly a formatting command, but it creates a useful popout window with the objects in it.
  In the gridview you can sort and filter through the output.

## Exercise X: Formats and Views

```powershell
Get-Command -Verb 'Format'
Get-Command -Verb 'Format' | Format-List
Get-Command -Verb 'Format' | Format-Table -Property Name, Source
Get-Command -Verb 'Format' | Format-Wide
Get-Command -Verb 'Format' | Out-GridView
```

Run the following commands to the right and answer the following questions:

1. How is the output view from `Format-List` different from the default?
2. How is the `Format-Table` output view different from the default?
3. What displays when you use `Format-Wide`?
4. Sort the output in the gridview on `Source`.
   Does that change anything?
