---
weight: 180
---

# Modules & the PSGallery

```powershell
Get-Module -ListAvailable
```
```text
    Directory: C:\WINDOWS\system32\WindowsPowerShell\v1.0\Modules
ModuleType Version    Name                ExportedCommands
---------- -------    ----                ----------------
Manifest   1.0.0.0    AppBackgroundTask   {Disable-AppBackgroundTaskDiagnosticLog, Enable-AppBackgroundTaskDiagnosticLog, S...
Manifest   2.0.0.0    AppLocker           {Get-AppLockerFileInformation, Get-AppLockerPolicy, New-AppLockerPolicy, Set-AppL...
Manifest   1.0.0.0    AppvClient          {Add-AppvClientConnectionGroup, Add-AppvClientPackage, Add-AppvPublishingServer, ...
...
```

To this point in the workshop, everything we've worked on has only included commands which are 'in the box', so to speak - the commands that ship with PowerShell itself.

Behind the scenes, all of those commands actually reside in _modules_ - logical containers for groups of commands.
Modules are how PowerShell code largely gets shipped around - there are additional modules available, written by Microsoft, by third parties (like VMWare), and by the community.

There is also a centralized location where publically available modules can be discovered: the [PowerShell Gallery][psgallery].
Here you can search for modules that might have commands or scripts which would be useful for you.

There's a set of commands for interacting both with the gallery and with PowerShell modules which you've downloaded and installed locally.

- `Get-Command -Module PowerShellGet`

## Exercise X: Modules

1. What command would you use to convert text from YAML format to PowerShell?
   Is that command available on your machine?
  - Remember the approved verbs list.
2. Use `Find-Command` to search the gallery for modules containing the appropriate command.
  - How many did it return?
3. Install the `powershell-yaml` module to your machine using `Install-Module`.
  - Make sure to set the `Scope` parameter to `CurrentUser`.
4. Run `Get-Command` again to search for the appropriate command.
  - Do you get different results?
5. Use `Get-Command` to return all of the commands whose noun is `Yaml`.
6. Save the module `PSYaml` to the path `~`.
7. Get the child items at `~/PSYaml` recursively.
  - What is there?
8. Use `Get-Command` to return all of the commands belonging to the `PSYaml` module.
  - Did anything return?
9. Run the following command to import the module into the current runspace:
  - `Import-Module -Name ~\PSYaml\1.0.2\PSYaml.psd1`
10. Use `Get-Command` to return all of the commands belonging to the `PSYaml` module again.
  - What are the results?
11. What is the value of the environment variable PSModule path?
    (Hint: you can split the variable into a readable list like this: `$env:PSModulePath.split(';')`)
12. Use `Remove-Module` to unload `PSYaml` from the current session.
13. Use the `Get-InstalledModule` command to check modules on your machine are installed from the gallery.
14. Use `Get-PSRepository` to investigate the default gallery information.
  - What properties and methods are available?
  - What's the publish location?

## Note on Getting Started

When you're first getting started, using these module commands may require you to install NuGet.
PowerShell will try to do this for you automatically.

For a more in depth look at getting started with the gallery, including installing PowerShellGet on older versions of Windows, see the [Microsoft docs][msdocs].

[psgallery]: https://www.powershellgallery.com/
[msdocs]:    https://docs.microsoft.com/en-us/powershell/gallery/installing-psget