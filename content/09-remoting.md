---
weight: 170
---

# Remoting

All of the PowerShell we've looked at so far relies on running on your local machine.
Eventually, however, it's probable that you will need to manage another machine.
To do so without RDPing to it will require you to use PowerShell remoting.

Note: In order to remote to a machine you _must_ have admin permissions on it.

You can interact with remote systems via PowerShell sessions, using the `*-PSSession*` commands.


## Interactive Remoting

```powershell
Enter-PSSession -ComputerName localhost
Exit-PSSession
```

Firstly, you can start interactive sessions - this is functionally the same as opening a PowerShell prompt on the target computer.
In the example we connect to the localhost, but in a real environment you could specify the FQDN of a server or the name of a server on the same domain as the client machine.
You can exit the session once you're done.
This is often useful if you need to look around on a target machine for multiple commands and explore a problem space.

## Invoke-Command

```powershell
$Host.Name
Invoke-Command -ComputerName localhost -ScriptBlock { $Host.Name }
```
```text
ConsoleHost
ServerRemoteHost
```

But what if you just need to send a single command and get the results?
If that's the case, you can use the `Invoke-Command` command to target one or more remote machines and execute PowerShell code on them.
The `ScriptBlock` parameter takes an arbitrary PowerShell block of code, similar to `Where-Object`.
It will return the output of whatever commands are specified in that scriptblock.

## Persistent Sessions

```powershell
$Session = New-PSSession -ComputerName localhost
Invoke-Command   -Session $Session -ScriptBlock { "One" }
Invoke-Command   -Session $Session -ScriptBlock { "Two" }
Remove-PSSession -Session $Session
```
```text
One
Two
```

You can also create a PowerShell session and save it to be used for multiple commands.
This cuts down on the overhead cost of creating and tearing down PowerShell remoting sessions - just remember to close them when you're done.

## Multiple Sessions

```powershell
New-PSSession -ContainerID ($env:Servers -Split ',') -OutVariable Sessions
```
```text
 Id Name            ComputerName    ComputerType    State         ConfigurationName     Availability
 -- ----            ------------    ------------    -----         -----------------     ------------
  7 Session7        8adadc56b28d... Container       Opened                                 Available
  8 Session8        9ca3c01950af... Container       Opened                                 Available
  6 Session6        6b1fd3588ea0... Container       Opened                                 Available
```

So far the examples have looked at managing a single remote host, but you can also manage several at once.
In this workshop we've prelaunched some docker containers and captured their container IDs for use as an environment variable.
PowerShell remoting has a neat built-in option (for Windows PowerShell 5.1+) to be able to manage containers on the host, no networking required, which is what we're using here.

<aside class="notice">
<strong>OutVariable</strong><br>
You can use the <code>OutVariable</code> parameter to allow the output of a command to go to the pipeline <em>and</em> to save it to a variable.
Be careful not to include a <code>$</code> in the name!
</aside>

```powershell
Invoke-Command -Session $Sessions -ScriptBlock { "This is $env:ComputerName" }
```
```text
This is 8ADADC56B28D
This is 9CA3C01950AF
This is 6B1FD3588EA0
```

Functionally, as far as running commands is concerned, all you need is an open PSSession - whether it's via WinRM remoting, SSH, or another methods doesn't effect how the commands are run.

There's more to discuss with remoting, such as [session configurations][session-config] and [implicit remoting][implicit-remoting], but those topics are beyond the scope of this workshop.

[session-config]:    https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_session_configurations?view=powershell-6
[implicit-remoting]: https://4sysops.com/archives/using-implicit-powershell-remoting-to-import-remote-modules/