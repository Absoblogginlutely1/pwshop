---
weight: 200
---

# What's Next?

You've learned a lot during this workshop!
At this point you've got most of the fundamentals of PowerShell in your hands.
You know how to find commands to do what you want, how to figure out how they work, how to introspect objects for additional methods and properties, how to filter objects, how to use the pipeline, how to assign variables and operate on them, how to use different providers, how to remotely manage machines and how to find commands that aren't available on your system, how to write scripts that use conditionals and loops and multiple output streams and how to handle errors.

This sets you up for a _lot_ of success.
Most of the rest of PowerShell is going to be a widening and deepening of the skills you've learned today - how to write better scripts, how to add documentation, how to write reusable code, how to package that code up for other folks to use.

To that end, here's a few challenges and a _lot_ of resources:

- Write a script to replace a manual task you currently have - clearing disk space, adding a user, restarting a service, finding out whether an app pool is running, whatever - just pick some task you currently do by hand or in the GUI and write a script to do it.
- Research and write a single advanced function using the resources below:
  - [About Advanced Functions (MS Docs)][msdocs-advanced-functions]
  - [Understanding Advanced Functions in Powershell][scripting-guys-functions]
  - [PowerShell Functions: Best Practices][pscookiemonster-advanced-functions]
- Use Pester to write tests for one script or function:
  - [Pester Wiki][pester-wiki]
  - [The Pester Book][pester-book]
- Use PowerShell Desired State Configuration to manage a service or application - doesn't have to be for production, just find something to configure for desired state.
  - [DSC Quick Start (MS Docs)][msdocs-dsc-quickstart]
  - [Getting Started with DSC Course (MS Virtual Academy)][mva-dsc]
  - [Learning PowerShell DSC][powershell-dsc-book]
- Read the following books:
  - [PowerShell 101][powershell-101]
  - [Learn Windows PowerShell in a Month of Lunches][powershell-month-of-lunches]
  - [Learn PowerShell Toolmaking in a Month of Lunches][toolmaking-month-of-lunches]

[msdocs-advanced-functions]:          https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_functions_advanced?view=powershell-6
[msdocs-dsc-quickstart]:              https://docs.microsoft.com/en-us/powershell/dsc/quickstart
[mva-dsc]:                            https://mva.microsoft.com/en-us/training-courses/getting-started-with-powershell-desired-state-configuration-dsc-8672
[pester-book]:                        https://leanpub.com/pesterbook
[powershell-101]:                     https://leanpub.com/powershell101
[powershell-dsc-book]:                https://www.amazon.com/Learning-PowerShell-DSC-deployment-configuration/dp/1787287246
[powershell-month-of-lunches]:        https://www.amazon.com/Learn-Windows-PowerShell-Month-Lunches/dp/1617291080
[pscookiemonster-advanced-functions]: http://ramblingcookiemonster.github.io/Building-PowerShell-Functions-Best-Practices/
[scripting-guys-functions]:           https://blogs.technet.microsoft.com/heyscriptingguy/2015/07/09/understanding-advanced-functions-in-powershell/
[toolmaking-month-of-lunches]:        https://www.amazon.com/Learn-PowerShell-Toolmaking-Month-Lunches/dp/1617291161
[pester-wiki]:                        https://github.com/pester/Pester/wiki/Pester
