---
weight: 160
---

# PowerShell Providers

```powershell
Get-ChildItem -Path ~
# Note: the next two commands will only work on Windows systems.
Get-ChildItem -Path 'HKCU:\Keyboard Layout\'
Get-ChildItem -Path 'Cert:\CurrentUser'
```
```text
    Directory: C:\Users
Mode                LastWriteTime         Length Name
----                -------------         ------ ----
d-----         5/5/2018   2:04 PM                Admin
d-----        9/23/2018   4:04 PM                Michael Lombardi
d-r---         5/5/2018   4:58 PM                Public

    Hive: HKEY_CURRENT_USER\Keyboard Layout
Name                           Property
----                           --------
Preload                        1 : 00000409
Substitutes
Toggle

Name : SmartCardRoot
Name : Root
Name : Trust
...
```

Providers are one of the ways PowerShell makes our lives easier - they allow us to use the [same set of commands][providers] to manage certificates, files, registry keys, and more - all without having to learn new commands for each.

We'll start with the first command in this group that you're likely to need: `Get-ChildItem`.
This command will search inside a container and return all of the objects it finds.
It is _not_ recursive by default, but does have the `Recurse` parameter to enable that behavior.

The same command works across three completely different providers - it returned a list of files/folders, then registry keys, then certificate stores.

Similarly, the rest of the `*-Item*` commands (use `Get-Command` to retrieve the full list) can be used to interact with those items.

## Exercise X: Files and Folders

1. Run the following commands:
  - `New-Item -Path ~/pwshopex -ItemType Directory`
  - `Push-Location -Path ~/pwshopex`
2. Notice that the command created a folder, not a file.
   What happens if you run the command, specifying `~/pwshopex/test` as the path, but do **not** specify an item type?
3. Get `~/pwshopex/test` - what properties and methods are available?
   What is the property for determining whether or not a file is read only?
4. Use the `Get-ItemProperty` command on `~/pwshopex/test` to retrieve whether or not the `test` file is read only.
5. Use the `Set-ItemProperty` command on `~/pwshopex/test` to set the read only property to `$true`.
   Retrieve the item property again - is it set to true?
4. Use the `Set-Item` command to modify `~/pwshopex/test` to have the text `apple`.
   Note the error that causes this to fail; not _all_ providers implement the same functionality.
5. For files, we have the `*-Content` commands.
   Use `Set-Content` to modify `~/pwshopex/test` to have the text `apple`.
   Why does this error?
6. Use `Set-ItemProperty` to set the file back to read only being false.
   Use `Set-Content` to modify `~/pwshopex/test` to have the text `apple`.
7. Use `Get-Content` to retrieve the text in `~/pwshopex/test`.
   What properties and methods are available?
   What is the object type?
8. Use `Add-Content` to add the string `banana` to `~/pwshopex/text`.
9. Use `Get-Content` to retrieve the text in `~/pwshopex/test` and pass the output through the pipeline to `Measure-Object`.
   Note that there are two objects - this is because PowerShell treats each line as a separate string.
10. Use `Clear-Content` to erase the text from `~/pwshopex/text`.
11. Rename `~/pwshopex/text` to `file`.
12. Copy `~/pwshopex/file` to `~/pwshopex/item`.
13. Use the command `Pop-Location`.
14. Use `Remove-Item` on `~/pwshopex`.

## Exercise X: Registry Keys

1. Run the following commands:
  - `New-Item -Path 'HKCU:\pwshop', 'HKCU:\pwshop\apple'`
  - `Push-Location -Path HKCU:\pwshop`
2. What properties and methods are available for the object representing `HKCU:\pwshop\apple`?
3. Use `New-ItemProperty to add the `Fruit` property to `HKCU:\pwshop\apple`, and set it to `$true`.
4. What properties are available for the object representing `HKCU:\pwshop\apple`?
   Is `Fruit` in the list of properties?
5. Use `Get-ItemProperty` on `HKCU:\pwshop\apple` - what properties are returned?
6. Copy `HKCU:\pwshop\apple` to `HKCU:\pwshop\biscuit`.
7. Set the value of `Fruit` on `HKCU:\pwshop\biscuit` to `$false`.
8. Return the list of all registry keys in `HKCU:\pwshop`.
9. Run the following commands:
  - `Pop-Location`
  - `Remove-Item -Path 'HKCU:\pwshop' -Recurse`

## Exercise X: Certificates

1. Run the following commands:
  - `Push-Location -Path Cert:\CurrentUser\My`
  - `New-SelfSignedCert -DnsName 'example.pwshop.local'`
2. What properties and methods are available on the object you just created?
   What is it's type?
3. Can you use `Set-ItemProperty` or `Set-Item` commands against this object?
4. Get the cert you created and pipe it to the following command:
  - `Export-Certificate -FilePath "$env:HOMEDRIVE$env:HOMEPATH/pwshop.cer"`
5. Open the newly created certificate file in the file explorer and compare its properties to the one in the store.
   You can do this with the following command:
  - `& explorer.exe "$env:HOMEDRIVE$env:HOMEPATH/pwshop.cer"`
6. Delete the self-signed cert from `Cert:\CurrentUser\My`.
7. What command would you use to import a certificate?
   Import the `pwshop.cer` certificate back into `CertL\CurrentUser\My`.
8. Delete the cert from both the store and the file system.

## Other Providers
```powershell
Get-PSProvider
```

There are also several other providers, both builtin and available via additional modules.
They can, like the three providers we've already explored, be interacted with through the `Item` commands.

[providers]: https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_providers?view=powershell-6
